package pentera

import (
	"testing"
)

const (
	filename = "../config.json"
	name     = "100"
)

func TestNewPenteraFromConfig(t *testing.T) {
	_, err := NewPentera(filename, name)
	if err != nil {
		t.Error(err)
	}
}

func TestNewRequest(t *testing.T) {
	p, _ := NewPentera(filename, name)

	_, err := p.newRequest("", "", nil)
	if err != nil {
		t.Error(err)
	}
}

func TestTasks(t *testing.T) {
	p, _ := NewPentera(filename, name)

	_, err := p.Tasks("62287674966b5ff7660458da")
	if err != nil {
		t.Error(err)
	}
}

func TestTasksAll(t *testing.T) {
	p, _ := NewPentera(filename, name)

	_, err := p.Tasks("")
	if err != nil {
		t.Error(err)
	}
}

func TestStats(t *testing.T) {
	p, _ := NewPentera(filename, name)

	_, err := p.Stats("62287674966b5ff7660458da")
	if err != nil {
		t.Error(err)
	}
}

func TestMitre(t *testing.T) {
	p, _ := NewPentera(filename, name)

	_, err := p.Mitre("62287674966b5ff7660458da")
	if err != nil {
		t.Error(err)
	}
}

func TestHost(t *testing.T) {
	p, _ := NewPentera(filename, name)

	_, err := p.Host("62287674966b5ff7660458da")
	if err != nil {
		t.Error(err)
	}
}
