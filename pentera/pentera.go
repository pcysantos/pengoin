package pentera

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
)

var (
	// Using a map makes it easier to reference different config blocks, but an anonymous
	// struct would be clearer.
	// The config structure is:
	// {
	// 	"name": {
	// 		"host": "...",
	// 		"client_id": "...",
	// 		"tgt": "..."
	// 	}
	// }
	config map[string]map[string]string
	wg     sync.WaitGroup
	save   chan string
)

type pentera struct {
	host     string
	clientID string
	tgt      string
}

// Tasks returns information about a task (assessment).
// If the taskID parameter is an empty string,
// a list of all tasks is returned instead.
func (p pentera) Tasks(taskID string) (*Tasks, error) {
	url := fmt.Sprintf("https://%s/api/v1/taskRun", p.host)
	if taskID != "" {
		url += "/" + taskID
	}

	req, err := p.newRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	tasks := Tasks{}

	if err = json.NewDecoder(resp.Body).Decode(&tasks); err != nil {
		return nil, err
	}

	return &tasks, nil
}

// Stats returns detailed information on a completed test such as vuln,
// achievements and host information.
func (p pentera) Stats(taskID string) (*Stats, error) {
	url := fmt.Sprintf("https://%s/api/v1/taskRun/%s/stats", p.host, taskID)

	req, err := p.newRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	stats := Stats{}

	if err = json.NewDecoder(resp.Body).Decode(&stats); err != nil {
		return nil, err
	}

	return &stats, nil
}

// Mitre returns information about a specific completed test.
func (p pentera) Mitre(taskID string) (*Mitre, error) {
	url := fmt.Sprintf("https://%s/api/v1/taskRun/%s/mitre", p.host, taskID)

	req, err := p.newRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	mitre := Mitre{}

	if err = json.NewDecoder(resp.Body).Decode(&mitre); err != nil {
		return nil, err
	}

	return &mitre, nil
}

// Host returns information from a specific completed test.
func (p pentera) Host(taskID string) (*Host, error) {
	url := fmt.Sprintf("https://%s/api/v1/taskRun/%s/host", p.host, taskID)

	req, err := p.newRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	host := Host{}

	if err = json.NewDecoder(resp.Body).Decode(&host); err != nil {
		return nil, err
	}

	return &host, nil
}

// newRequest will return an http.Request with the proper basic authentication header.
// Pentera uses basic auth for API requests, but each request needs a new access token
// to use as the username (the password is empty).
func (p pentera) newRequest(method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	// Get a request token.
	authURL := fmt.Sprintf("https://%s/auth/token", p.host)
	authBody := fmt.Sprintf(`{"client_id": "%s", "tgt": "%s"}`, p.clientID, p.tgt)
	resp, err := http.Post(authURL, "application/json", bytes.NewBufferString(authBody))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode > 299 {
		return nil, fmt.Errorf("invalid HTTP response: %s", resp.Status)
	}

	authResp := struct {
		TGT   string `json:"tgt"`
		Token string `json:"token"`
	}{}

	if err = json.NewDecoder(resp.Body).Decode(&authResp); err != nil {
		return nil, err
	}

	// TGT changes after each request.
	wg.Add(1)
	go func() {
		save <- authResp.TGT
	}()
	wg.Wait()

	// The token becomes the username and the password is blank.
	req.SetBasicAuth(authResp.Token, "")

	return req, nil
}

// NewPentera returns a new instance of Pentera by reading a config file and
// selecting the chosen connection.
func NewPentera(filename, name string) (*pentera, error) {
	configFile, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	// config is a global map[string]map[string]string
	if err := json.Unmarshal(configFile, &config); err != nil {
		return nil, err
	}

	// Wait for save signal.
	go func() {
		// In theory we only need to save this once, but we'll put it on
		// a loop in case we want to make multiple requests in the future.
		for {
			config[name]["tgt"] = <-save

			data, err := json.MarshalIndent(config, "", "\t")
			if err != nil {
				log.Fatal("unable to read config map")
			}

			if err := ioutil.WriteFile(filename, data, 0644); err != nil {
				log.Fatal("unable to save config file")
			}

			wg.Done()
		}
	}()

	return &pentera{config[name]["host"], config[name]["client_id"], config[name]["tgt"]}, nil
}

func init() {
	save = make(chan string)

	// Disable SSL certificate check.
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
}
