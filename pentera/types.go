package pentera

type Meta struct {
	ServerTime  float64 `json:"serverTime"`
	AgentsReady bool    `json:"agentsReady"`
	Status      string  `json:"status"`
	Token       string  `json:"token"`
	User        struct {
		UserName string `json:"userName"`
		Role     string `json:"role"`
	} `json:"user"`
}

type Tasks struct {
	Meta     `json:"meta"`
	TaskRuns []struct {
		TaskRunID                  string  `json:"taskRunId"`
		TemplateType               string  `json:"templateType"`
		TaskRunName                string  `json:"taskRunName"`
		TemplateID                 string  `json:"templateId"`
		TemplateMode               string  `json:"templateMode"`
		AllowExploits              bool    `json:"allowExploits"`
		IsRunnable                 bool    `json:"isRunnable"`
		IsCompletionStatusExists   bool    `json:"isCompletionStatusExists"`
		StartTime                  float64 `json:"startTime"`
		EndTime                    float64 `json:"endTime"`
		DurationBySeconds          int     `json:"durationBySeconds"`
		ThreatLevel                float64 `json:"threatLevel"`
		VulThreatLevel             float64 `json:"vulThreatLevel"`
		AchThreatLevel             float64 `json:"achThreatLevel"`
		NumHosts                   int     `json:"numHosts"`
		NumVulnerabilities         int     `json:"numVulnerabilities"`
		NumCriticalVulnerabilities int     `json:"numCriticalVulnerabilities"`
		Status                     string  `json:"status"`
		SingleActionType           string  `json:"singleActionType,omitempty"`
	} `json:"taskRuns"`
}

type Stats struct {
	Meta         `json:"meta"`
	TaskRunStats struct {
		TaskRunID                string  `json:"taskRunId"`
		TemplateType             string  `json:"templateType"`
		TaskRunName              string  `json:"taskRunName"`
		TemplateID               string  `json:"templateId"`
		TemplateMode             string  `json:"templateMode"`
		AllowExploits            bool    `json:"allowExploits"`
		IsRunnable               bool    `json:"isRunnable"`
		IsCompletionStatusExists bool    `json:"isCompletionStatusExists"`
		StartTime                float64 `json:"startTime"`
		EndTime                  float64 `json:"endTime"`
		DurationBySeconds        int     `json:"durationBySeconds"`
		ThreatLevel              float64 `json:"threatLevel"`
		VulThreatLevel           float64 `json:"vulThreatLevel"`
		AchThreatLevel           float64 `json:"achThreatLevel"`
		NumHosts                 int     `json:"numHosts"`
		NumVulnerabilities       int     `json:"numVulnerabilities"`
		Status                   string  `json:"status"`
		Vulnerabilities          []struct {
			VulID              string      `json:"vulId"`
			AchID              string      `json:"achId"`
			Name               string      `json:"name"`
			Severity           float64     `json:"severity"`
			SourceDescription  string      `json:"sourceDescription"`
			Show               bool        `json:"show"`
			IsVul              bool        `json:"isVul"`
			AchievementType    interface{} `json:"achievementType"`
			AffectedHostIds    []string    `json:"affectedHostIds"`
			Insight            string      `json:"insight"`
			Immunity           string      `json:"immunity"`
			Relevancy          string      `json:"relevancy"`
			RelevancyShortForm string      `json:"relevancyShortForm"`
			Description        string      `json:"description"`
		} `json:"vulnerabilities"`
		NumVulnerableHosts     int `json:"numVulnerableHosts"`
		VulnerabilitiesByLevel []struct {
			VulLevel      int    `json:"vulLevel"`
			VulLevelTitle string `json:"vulLevelTitle"`
			Count         int    `json:"count"`
		} `json:"vulnerabilitiesByLevel"`
		AchivementsByLevel []struct {
			AchivLevel      int    `json:"achivLevel"`
			AchivLevelTitle string `json:"achivLevelTitle"`
			Count           int    `json:"count"`
		} `json:"achivementsByLevel"`
		ActivitiesStatusByCategory []struct {
			Category   string `json:"category"`
			Message    string `json:"message"`
			Count      int    `json:"count"`
			InProgress bool   `json:"inProgress"`
		} `json:"activitiesStatusByCategory"`
		ActivitiesStatus []interface{} `json:"activitiesStatus"`
		Hosts            []struct {
			HostID                    string      `json:"hostId"`
			Ipv4                      string      `json:"ipv4"`
			Mac                       interface{} `json:"mac"`
			Vendor                    interface{} `json:"vendor"`
			Domain                    string      `json:"domain"`
			OsName                    string      `json:"osName"`
			DNSNamesStr               string      `json:"dnsNamesStr"`
			OsType                    string      `json:"osType"`
			IsServer                  bool        `json:"isServer"`
			HighestVulnerabilityLevel string      `json:"highestVulnerabilityLevel"`
			IsAttacked                bool        `json:"isAttacked"`
			IsInRange                 bool        `json:"isInRange"`
			MarkedForScreenshot       bool        `json:"markedForScreenshot"`
			ScreenshotInitiated       int         `json:"screenshotInitiated"`
			Roles                     []string    `json:"roles"`
			NumVulnerabilities        int         `json:"numVulnerabilities"`
			ConfidenceLevel           int         `json:"confidenceLevel"`
			AntiVirusList             interface{} `json:"antiVirusList"`
		} `json:"hosts"`
		ActionsForApproval []struct {
			ID             string  `json:"id"`
			DisplayName    string  `json:"displayName"`
			Target         string  `json:"target"`
			ApprovalStatus string  `json:"approvalStatus"`
			Created        float64 `json:"created"`
			Description    string  `json:"description"`
			ApprovedBy     string  `json:"approvedBy"`
			ApprovalTime   string  `json:"approvalTime"`
			Family         string  `json:"family"`
			Hostname       string  `json:"hostname"`
			Os             string  `json:"os"`
		} `json:"actionsForApproval"`
		Achievements []struct {
			VulID              string      `json:"vulId"`
			AchID              string      `json:"achId"`
			Name               string      `json:"name"`
			Severity           float64     `json:"severity"`
			SourceDescription  string      `json:"sourceDescription"`
			Show               bool        `json:"show"`
			IsVul              bool        `json:"isVul"`
			AchievementType    interface{} `json:"achievementType"`
			AffectedHostIds    []string    `json:"affectedHostIds"`
			Insight            string      `json:"insight"`
			Immunity           interface{} `json:"immunity"`
			Relevancy          interface{} `json:"relevancy"`
			RelevancyShortForm interface{} `json:"relevancyShortForm"`
			Description        string      `json:"description"`
		} `json:"achievements"`
		DevicesSummary struct {
			DiscoveredDevices int `json:"discoveredDevices"`
			TotalHosts        struct {
				WinWorkstation int `json:"winWorkstation"`
				WinServer      int `json:"winServer"`
				GenericWindows int `json:"genericWindows"`
				Linux          int `json:"linux"`
				NetworkDevices int `json:"networkDevices"`
				Other          int `json:"other"`
			} `json:"totalHosts"`
			HostSeverityLevel struct {
				Critical int `json:"critical"`
				High     int `json:"high"`
				Medium   int `json:"medium"`
				Low      int `json:"low"`
			} `json:"hostSeverityLevel"`
		} `json:"devicesSummary"`
		HasUserInput bool `json:"hasUserInput"`
	} `json:"taskRunStats"`
}

type Mitre struct {
	Meta  `json:"meta"`
	Mitre struct {
		Categories []struct {
			CategoryName string `json:"categoryName"`
			Techniques   []struct {
				Technique              string  `json:"technique"`
				TotalActions           int     `json:"totalActions"`
				TotalSuccessfulActions int     `json:"totalSuccessfulActions"`
				Severity               float64 `json:"severity"`
				ID                     string  `json:"id"`
				SubTechniques          []struct {
					Technique              string  `json:"technique"`
					TotalActions           int     `json:"totalActions"`
					TotalSuccessfulActions int     `json:"totalSuccessfulActions"`
					Severity               float64 `json:"severity"`
					ID                     string  `json:"id"`
				} `json:"subTechniques,omitempty"`
			} `json:"techniques"`
		} `json:"categories"`
	} `json:"mitre"`
	Response int `json:"response"`
}

type Host struct {
	Meta  `json:"meta"`
	Hosts []struct {
		HostID                    string      `json:"hostId"`
		Ipv4                      string      `json:"ipv4"`
		Mac                       interface{} `json:"mac"`
		Vendor                    interface{} `json:"vendor"`
		Domain                    string      `json:"domain"`
		OsName                    string      `json:"osName"`
		DNSNamesStr               string      `json:"dnsNamesStr"`
		OsType                    string      `json:"osType"`
		IsServer                  bool        `json:"isServer"`
		HighestVulnerabilityLevel string      `json:"highestVulnerabilityLevel"`
		IsAttacked                interface{} `json:"isAttacked"`
		IsInRange                 bool        `json:"isInRange"`
		MarkedForScreenshot       bool        `json:"markedForScreenshot"`
		ScreenshotInitiated       int         `json:"screenshotInitiated"`
		Roles                     []string    `json:"roles"`
		NumVulnerabilities        int         `json:"numVulnerabilities"`
		ConfidenceLevel           int         `json:"confidenceLevel"`
		AntiVirusList             interface{} `json:"antiVirusList"`
		Interfaces                int         `json:"interfaces"`
		SoftwareServices          int         `json:"softwareServices"`
		Software                  int         `json:"software"`
		Processes                 int         `json:"processes"`
		Kbs                       int         `json:"kbs"`
		NumPorts                  int         `json:"numPorts"`
		Services                  []struct {
			ServiceName string      `json:"serviceName"`
			Port        int         `json:"port"`
			Transport   string      `json:"transport"`
			State       string      `json:"state"`
			Version     interface{} `json:"version"`
		} `json:"services"`
		IsCriticalAssets bool `json:"isCriticalAssets"`
		RceSucceed       bool `json:"rceSucceed"`
	} `json:"hosts"`
}
